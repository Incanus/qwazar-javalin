import { Link, Route, Router, Routes, useParams } from '@solidjs/router';
import { Container } from 'solid-bootstrap';
import { Component, createResource, ErrorBoundary, For, Show } from 'solid-js';
import { getView, getViewsets } from './api';
import { LinkWidget, renderWidget, WidgetProps } from './widgets';

const View: Component = () => {
  const params = useParams()
  const viewset = () => params.viewset ?? 'home'
  const view = () => params.view ?? 'index'

  // TODO: handle view not found
  const [data] = createResource(
    () => ({ viewset: viewset(), view: view() }),
    (params) => getView(params.viewset, params.view),
  )

  return (
    <ErrorBoundary
      fallback={(err, reset) => <div onClick={reset}>{err.toString()}</div>}
    >
      <p>rendering view {view} of viewset {viewset}:</p>
      <Show when={!data.loading} fallback={<p>načítám</p>}>
        {renderWidget(data() as WidgetProps)}
      </Show>
    </ErrorBoundary >
  )
}

const ViewRoutes: Component = (props) => {
  return (
    <Routes>
      <Route path="/" component={View} />
      <Route path="/:viewset" component={View} />
      <Route path="/:viewset/:view" component={View} />
    </Routes>
  )
}

const ViewLinks: Component = (props) => {
  const [data] = createResource(getViewsets)
  const viewsets = () => data() ?? []

  return (
    <Show when={!data.loading} fallback={<p>načítám</p>}>
      <p>viewsets data: {JSON.stringify(data())}</p>

      <ul>
        <li><Link href="/">/</Link></li>
        <For each={viewsets()}>{(viewset) =>
          <For each={viewset.viewNames}>{(viewName) =>
            <li><LinkWidget viewsetName={viewset.name} viewName={viewName} /></li>
          }</For>
        }</For>
      </ul>
    </Show>
  )
}

const App: Component = () => {
  return (
    <Container>
        <Router>
          <ViewLinks />
          <ViewRoutes />
        </Router>
    </Container>
  );
};

export default App;
