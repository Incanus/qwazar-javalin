export type Viewset = {
    name: string,
    viewNames: string[],
}

export type View = {
    name: string,
}
