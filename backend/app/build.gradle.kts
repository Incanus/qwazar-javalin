group = "cz.incanus.qwazar"
version = "1.0-SNAPSHOT"

plugins {
    application
}

dependencies {
    val koinVersion = "3.2.2"
    val koinAnnotationsVersion = "1.0.3"

    implementation(project(":core"))
    implementation(project(":web"))

    implementation("io.insert-koin:koin-core:$koinVersion")
    implementation("io.insert-koin:koin-annotations:$koinAnnotationsVersion")

    implementation("io.javalin:javalin-bundle:5.0.1")
    implementation("org.slf4j:slf4j-simple:2.0.3")

    testImplementation("io.insert-koin:koin-test-jvm:$koinVersion")
}

application {
    mainClass.set("cz.incanus.qwazar.app.MainKt")
}
