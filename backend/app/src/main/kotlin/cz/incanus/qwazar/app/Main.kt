package cz.incanus.qwazar.app

import com.fasterxml.jackson.core.JacksonException
import cz.incanus.qwazar.app.viewsets.HomeViewset
import cz.incanus.qwazar.core.viewsets.ViewsetRegistry
import cz.incanus.qwazar.web.common.BadRequest
import cz.incanus.qwazar.web.common.NotFound
import cz.incanus.qwazar.web.viewsets.ViewsetRouter
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.http.HttpStatus

// TODO: add container widgets, at least a list
// TODO: the home/index view should render a list of links for all views

val app: Javalin = Javalin
    .create { config ->
        config.showJavalinBanner = false
        config.http.defaultContentType = "application/json"

        config.plugins.enableDevLogging()
        config.plugins.enableCors { cors ->
            cors.add { it.anyHost() }
        }
    }
    .routes {
        get("/") { ctx -> ctx.json(listOf("viewsets")) }

        get<ViewsetRouter>().routes()
    }
    .exception(NotFound::class.java) { error, ctx ->
        ctx.status(HttpStatus.NOT_FOUND).json(mapOf("error" to error.message))
    }
    .exception(BadRequest::class.java) { error, ctx ->
        ctx.status(HttpStatus.BAD_REQUEST).json(mapOf("error" to error.message))
    }
    .exception(JacksonException::class.java) { error, ctx ->
        ctx.status(HttpStatus.BAD_REQUEST).json(mapOf("error" to error.message))
    }

fun registerViewsets() {
    val registry = get<ViewsetRegistry>()

    registry.register(HomeViewset)
}

fun main() {
    registerViewsets()

    app.start(7070)
}
