package cz.incanus.qwazar.app.viewsets

import cz.incanus.qwazar.core.views.Link
import cz.incanus.qwazar.core.views.Text
import cz.incanus.qwazar.core.viewsets.Viewset

val HomeViewset = Viewset("home") {
    view("index") {
        Link("home", "other")
    }

    view("other") {
        Text("asdf")
    }
}
