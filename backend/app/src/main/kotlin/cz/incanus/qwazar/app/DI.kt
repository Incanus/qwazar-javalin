package cz.incanus.qwazar.app

import cz.incanus.qwazar.core.viewsets.ViewsetRegistry
import cz.incanus.qwazar.web.viewsets.ViewsetRouter
import org.koin.dsl.koinApplication
import org.koin.dsl.module

val di = koinApplication {
    val app = module {
        single { ViewsetRegistry() }
        single { ViewsetRouter(get()) }
    }

    modules(app)
}.koin

inline fun <reified T : Any> get() = di.get<T>()
inline fun <reified T : Any> inject() = di.inject<T>()
