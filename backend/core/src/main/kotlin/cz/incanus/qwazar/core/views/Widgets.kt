package cz.incanus.qwazar.core.views

import cz.incanus.qwazar.core.viewsets.Viewset

open class Widget(val type: String)
class Text(val content: String) : Widget("text")
class Button(val label: String) : Widget("button")
class Link(val viewsetName: String, val viewName: String) : Widget("link") {
    constructor(viewset: Viewset, viewName: String) : this(viewset.name, viewName)
}
