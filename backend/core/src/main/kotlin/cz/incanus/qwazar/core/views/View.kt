package cz.incanus.qwazar.core.views

class ViewContext

typealias ViewFunction = (context: ViewContext) -> Widget
