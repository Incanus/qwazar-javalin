package cz.incanus.qwazar.core.viewsets

class ViewsetRegistry {
    private val viewsets = mutableMapOf<String, Viewset>()

    operator fun get(name: String) = viewsets[name]

    fun all() = viewsets.values.toList()

    fun register(viewset: Viewset) {
        viewsets[viewset.name] = viewset
    }
}
