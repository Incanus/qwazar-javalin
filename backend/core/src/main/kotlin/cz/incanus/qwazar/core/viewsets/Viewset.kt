package cz.incanus.qwazar.core.viewsets

import cz.incanus.qwazar.core.views.ViewContext
import cz.incanus.qwazar.core.views.ViewFunction
import kotlin.collections.set

open class Viewset(val name: String) {
    private val views = mutableMapOf<String, ViewFunction>()

    val viewNames get() = views.keys

    fun view(viewName: String, render: ViewFunction) {
        if (viewName in views) {
            throw IllegalArgumentException("viewset ${this.name} already has a view named $viewName")
        }

        views[viewName] = render
    }

    fun render(viewName: String, context: ViewContext = ViewContext()) = getView(viewName)(context)

    private fun getView(viewName: String) = views[viewName]
        ?: throw NoSuchElementException("viewset ${this.name} has no view named $viewName")

    companion object {
        operator fun invoke(name: String, init: Viewset.() -> Unit): Viewset {
            return Viewset(name).apply(init)
        }
    }
}
