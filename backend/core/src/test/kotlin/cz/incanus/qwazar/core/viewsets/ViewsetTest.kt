package cz.incanus.qwazar.core.viewsets

import cz.incanus.qwazar.core.views.Link
import cz.incanus.qwazar.core.views.Text
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.beInstanceOf
import org.junit.jupiter.api.Test

const val HOME = "home"
const val INDEX = "index"
const val OTHER = "other"

val HomeViewset = Viewset(HOME) {
    view(INDEX) {
        Link(this, OTHER)
    }

    view(OTHER) {
        Text("asdf")
    }
}

class ViewsetTest {
    @Test
    fun itWorks() {
        val widget = HomeViewset.render(INDEX)

        widget should beInstanceOf<Link>()
        val link = widget as Link
        link.viewsetName shouldBe HOME
        link.viewName shouldBe OTHER
    }
}
