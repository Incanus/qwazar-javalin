package cz.incanus.qwazar.web.viewsets

import cz.incanus.qwazar.core.viewsets.ViewsetRegistry
import cz.incanus.qwazar.web.common.Router
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path

class ViewsetRouter(private val viewsets: ViewsetRegistry) : Router() {
    fun routes() {
        path("/viewsets") {
            get {
                ViewsetHandler(it, viewsets).viewsets()
            }

            path(ViewsetHandler.viewsetNameParam.placeholder) {
                get {
                    ViewsetHandler(it, viewsets).viewset()
                }

                path("views") {
                    get {
                        ViewsetHandler(it, viewsets).views()
                    }

                    path(ViewsetHandler.viewNameParam.placeholder) {
                        get("") {
                            ViewsetHandler(it, viewsets).renderView()
                        }
                    }
                }
            }
        }
    }
}
