package cz.incanus.qwazar.web.viewsets

import cz.incanus.qwazar.core.viewsets.ViewsetRegistry
import cz.incanus.qwazar.web.common.Handler
import io.javalin.http.Context

class ViewsetHandler(
    context: Context,
    private val viewsets: ViewsetRegistry,
) : Handler(context) {
    companion object {
        const val viewNameParam = "viewName"
        const val viewsetNameParam = "viewsetName"
    }

    fun viewsets() {
        json(viewsets.all())
    }

    fun viewset() {
        json(viewset)
    }

    fun views() {
        json(viewset.viewNames)
    }

    fun renderView() {
        json(viewset.render(viewName))
    }

    private val viewset by lazy {
        viewsets[viewsetName] ?: notFound("Viewset $viewsetName not found")
    }

    private val viewName by lazy { context.pathParam(viewNameParam) }
    private val viewsetName by lazy { context.pathParam(viewsetNameParam) }
}
