package cz.incanus.qwazar.web.common

open class Router {
    val String.placeholder get() = "{$this}"
}
