package cz.incanus.qwazar.web.viewsets

import cz.incanus.qwazar.core.views.Link
import cz.incanus.qwazar.core.views.Text
import cz.incanus.qwazar.core.viewsets.Viewset
import cz.incanus.qwazar.core.viewsets.ViewsetRegistry
import io.javalin.Javalin
import io.javalin.http.HttpStatus
import io.javalin.json.JavalinJackson
import io.javalin.json.toJsonString
import io.javalin.testtools.JavalinTest
import io.javalin.testtools.TestConfig
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

val HomeViewset = Viewset("home") {
    view("index") {
        Link("home", "other")
    }

    view("other") {
        Text("asdf")
    }
}

val viewsets = ViewsetRegistry()

val app: Javalin = Javalin.create().routes {
    ViewsetRouter(viewsets).routes()
}

class ViewsetsEndpointTest {
    @BeforeEach
    fun setUp() {
        viewsets.register(HomeViewset)
    }

    @Test
    fun itWorks() {
        JavalinTest.test(app, TestConfig(captureLogs = false)) { _, client ->
            val response = client.get("/viewsets/${HomeViewset.name}/views/index")

            response.code shouldBe HttpStatus.OK.code
            response.body!!.string() shouldBe JavalinJackson().toJsonString(
                Link(HomeViewset, "other"),
            )
        }
    }
}
