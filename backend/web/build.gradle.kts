group = "cz.incanus.qwazar"
version = "1.0-SNAPSHOT"

dependencies {
    implementation(project(":core"))

    implementation("io.javalin:javalin:5.0.1")
    implementation("io.javalin:javalin-testtools:5.0.1")
}
