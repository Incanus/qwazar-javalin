import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "cz.incanus.qwazar"
version = "1.0-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.7.20"
    id("com.adarshr.test-logger") version "3.2.0" apply(false)
    id("org.jlleitschuh.gradle.ktlint") version "10.3.0" apply(false)
}

subprojects {
    apply(plugin = "java")
    apply(plugin = "kotlin")
    apply(plugin = "com.adarshr.test-logger")
    apply(plugin = "org.jlleitschuh.gradle.ktlint")

    repositories {
        mavenCentral()
    }

    dependencies {
        val jupiterVersion = "5.9.0"

        implementation(kotlin("stdlib-jdk8"))

        // this pulls in kotlin-test-junit5, but koin-test pulls in kotlin-test-junit and these are in conflict
        // testImplementation(kotlin("test"))
        testImplementation("org.junit.jupiter:junit-jupiter-api:$jupiterVersion")
        testImplementation("io.kotest:kotest-assertions-core-jvm:5.5.0")

        testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$jupiterVersion")
    }

    tasks.test {
        useJUnitPlatform()
    }

    tasks.withType<KotlinCompile> {
        // kotlinOptions.jvmTarget = "1.8"
        kotlinOptions.jvmTarget = "17"
    }
}
